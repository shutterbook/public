Privacy Policy

This Privacy Policy ("Policy") describes how Shutterbook ("Company", "we", "us", or "our") collects, uses, and discloses personal information when you use our website shutterbook.app ("Website") and the services ("Services") provided through it. By accessing or using the Website or Services, you ("User", "you", or "your") agree to the terms of this Policy. If you do not agree with this Policy, please do not use our Services.

1. Information We Collect

1.1. Information You Provide: When you register an account, subscribe to our services, or communicate with us, we may collect personal information such as your name, email address, phone number, and billing information.

1.2. Automatically Collected Information: We may collect certain information automatically when you use our Website, including your IP address, browser type, operating system, and browsing behavior.

1.3. Cookies: We use cookies and similar tracking technologies to analyze trends, administer the Website, track users' movements around the Website, and gather demographic information about our user base. You can control the use of cookies through your browser settings.

2. How We Use Your Information

2.1. Providing Services: We use the information we collect to provide and maintain our Services, including connecting Property Owners with Photographers, processing payments, and managing user accounts.

2.2. Communication: We may use your contact information to communicate with you about your account, updates to our Services, and promotional offers. You can opt-out of promotional communications at any time.

2.3. Improving Our Services: We may use aggregated data to analyze trends, monitor the usage of our Services, and improve the user experience.

3. Sharing of Information

3.1. Third-Party Service Providers: We may share your personal information with third-party service providers who assist us in providing our Services, such as payment processors, hosting providers, and customer support services.

3.2. Legal Requirements: We may disclose your information if required to do so by law or in response to valid legal requests, such as subpoenas or court orders.

3.3. Business Transfers: In the event of a merger, acquisition, or sale of all or a portion of our assets, your information may be transferred as part of the transaction. You will be notified via email and/or a prominent notice on our Website of any change in ownership or uses of your personal information.

4. Data Security

4.1. We take reasonable measures to protect your personal information from unauthorized access, disclosure, alteration, or destruction. However, no method of transmission over the internet or electronic storage is 100% secure, and we cannot guarantee absolute security.

5. Your Rights

5.1. You have the right to access, correct, or delete your personal information. You may also request that we restrict the processing of your information or object to its processing.

5.2. If you have any questions about your rights or would like to exercise them, please contact us using the information provided below.

6. Children's Privacy

6.1. Our Services are not directed to individuals under the age of 18. We do not knowingly collect personal information from children. If you are a parent or guardian and believe that your child has provided us with personal information, please contact us, and we will take steps to remove that information from our systems.

7. Changes to This Policy

7.1. We reserve the right to update or change this Policy at any time. Any changes will be effective immediately upon posting the revised Policy on our Website. Your continued use of the Services after any such changes constitutes acceptance of the new Policy.

8. Contact Us

8.1. If you have any questions about this Privacy Policy, please contact us at hello@shutterbook.app
By using our Services, you acknowledge that you have read, understood, and agree to be bound by this Privacy Policy.

