Terms and Conditions

These Terms and Conditions ("Agreement") govern the use of the services ("Services") provided by Shutterbook ("Company", "we", "us", or "our") through our website [Your Website URL] ("Website"). By accessing or using the Website or Services, you ("User", "you", or "your") agree to be bound by this Agreement. If you do not agree to these terms, please do not use our Services.

1. Services

1.1. Our Website acts as a platform connecting property owners seeking photography services ("Property Owners") with professional photographers offering their services ("Photographers").

1.2. We offer subscription-based plans for both Property Owners and Photographers:
a. The Free Plan provides limited access to our platform's features.
b. The Pro Plan offers additional features and benefits for a subscription fee.

1.3. We do not endorse any Photographer or Property Owner, and we are not responsible for the quality of services provided.

2. User Responsibilities

2.1. Property Owners:
a. You are responsible for verifying the credentials, qualifications, and reliability of Photographers.
b. You agree to provide accurate information about your property and your requirements.
c. Pro Plan users are entitled to additional benefits as outlined in the subscription plan.

2.2. Photographers:
a. You must provide accurate information about your services, pricing, and availability.
b. You agree to deliver high-quality services as per the agreed terms with Property Owners.
c. Pro Plan users are entitled to additional benefits as outlined in the subscription plan.

3. Subscription Plans and Payments

3.1. Property Owners:
a. You may choose between the Free Plan and the Pro Plan for accessing our platform's services.
b. Subscription fees for the Pro Plan are payable monthly or annually, as per the selected billing cycle.
c. Payment processing is handled securely through our third-party payment processor.

3.2. Photographers:
a. You may choose between the Free Plan and the Pro Plan for accessing our platform's services.
b. Subscription fees for the Pro Plan are payable monthly or annually, as per the selected billing cycle.
c. Payment processing is handled securely through our third-party payment processor.

4. Intellectual Property

4.1. All content, including but not limited to text, images, logos, and trademarks, on the Website is the property of the Company or its licensors.

4.2. Users retain ownership of their content uploaded to the Website. By using our Services, you grant us a non-exclusive, worldwide, royalty-free license to use, reproduce, and display your content for the purpose of providing the Services.

5. Limitation of Liability

5.1. We do not guarantee the accuracy, reliability, or suitability of the Services for any particular purpose.

5.2. We are not liable for any direct, indirect, incidental, consequential, or punitive damages arising out of your use of the Services.

6. Termination

6.1. We reserve the right to suspend or terminate your access to the Services at any time without prior notice if you violate this Agreement or for any other reason at our discretion.

7. Governing Law

7.1. This Agreement shall be governed by and construed in accordance with the laws of Your Jurisdiction.

8. Changes to Terms

8.1. We reserve the right to modify or replace these terms at any time. Any changes will be effective immediately upon posting on the Website. Your continued use of the Services after any such changes constitutes acceptance of the new terms.

9. Contact Us

9.1. If you have any questions about these Terms and Conditions, please contact us at hello@shutterbook.app .

By using our Services, you acknowledge that you have read, understood, and agree to be bound by these Terms and Conditions.

